﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Conta
    {
        public void operaSaida(string tipo, decimal valor, Historico d, List<Historico> lista)
        {
            if (tipo == "R$")
            {
                d.Valor = d.Valor - valor;
                var operacao = new Historico()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Simbolo = "R$",
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
            if (tipo == "$")
            {
                d.Valor = d.Valor - valor;
                var operacao = new Historico()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Simbolo = "$",
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
        }

        public void operaEntrada(string tipo, decimal valor, Historico d, List<Historico> lista)
        {
            if (tipo == "R$")
            {
                d.Valor = valor + d.Valor;
                var operacao = new Historico()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Simbolo = "R$",
                    TipoOperacao = "Entrada"
                };
                lista.Add(operacao);
            }

            if (tipo == "$")
            {
                d.Valor = valor + d.Valor;
                var operacao = new Historico()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Simbolo = "$",
                    TipoOperacao = "Entrada"
                };
                lista.Add(operacao);
            }
        }

        public decimal checaSaldo(string tipo, Historico meuSaldoReal, Historico meuSaldoDollar)
        {
            if (tipo == "$")
                return meuSaldoDollar.Valor;
            else
                return meuSaldoReal.Valor;
        }
    }
}
