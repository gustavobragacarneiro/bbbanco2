﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Historico
    {
        public DateTime Data { get; set; }
        public decimal Cotacao { get; set; }
        public decimal Valor { get; set; }
        public String TipoOperacao { get; set; }
        public String Simbolo { get; set; }
        public String Tipo { get; set; }

        public void geraHistorico(List<Historico> lista)
        {
            var operacoesEntrada = lista.Where(c => c.TipoOperacao == "Entrada");
            var operacoesSaida = lista.Where(c => c.TipoOperacao == "Saída");
            var operacoesConversaoRealDollar = lista.Where(c => c.TipoOperacao == "Conversao(Real/Dollar)");
            var operacoesConversaoDollarReal = lista.Where(c => c.TipoOperacao == "Conversao(Dollar/Real)");

            foreach (Historico deposito in operacoesEntrada)
            {
                Console.Write("Estes são seus depósitos: ");
                Console.WriteLine(deposito.Simbolo + "" + deposito.Valor + ", " + deposito.Data);
            }

            foreach (Historico saque in operacoesSaida)
            {
                Console.Write("Estes são seus saques: ");
                Console.WriteLine(saque.Simbolo + "" + saque.Valor + ", " + saque.Data);
            }

            foreach (Historico conversao in operacoesConversaoRealDollar)
            {
                Console.Write("Estas são suas conversões de Real para Dóllar: ");
                Console.WriteLine(conversao.Simbolo + ", " + conversao.Data + ", " + conversao.Cotacao);
            }

            foreach (Historico conversao in operacoesConversaoDollarReal)
            {
                Console.Write("Estas são suas conversões de Dóllar para Real: ");
                Console.WriteLine(conversao.Simbolo + ", " + conversao.Data + ", " + conversao.Cotacao);
            }
        }
    }
}
