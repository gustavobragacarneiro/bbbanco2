﻿using System.Collections.ObjectModel;
using Jarloo.CardStock.Models;
using System.Xml.Linq;
using System.Collections.Generic;
using Entities;
using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var meuSaldoDollar = new Historico();
            var operacao = new Conta();
            var minhaLista = new List<Historico>();
            var meuHistorico = new Historico();
            var conversor = new Conversor();


            #region Cocastro
            operacao.operaEntrada("$", 50M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("R$", 100M, meuSaldoDollar, minhaLista);

            operacao.operaSaida("R$", 100M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("R$", 200M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("$", 50M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("R$", 50M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("$", 300.00M, meuSaldoDollar, minhaLista);

            operacao.operaEntrada("R$", 300.00M, meuSaldoDollar, minhaLista);

            conversor.operaConversao("$", 300.00M, meuSaldoDollar, minhaLista);

            conversor.operaConversao("R$", 300.00M, meuSaldoDollar, minhaLista);

            operacao.operaSaida("$", 100.00M, meuSaldoDollar, minhaLista);
            #endregion

            meuHistorico.geraHistorico(minhaLista);

            Console.ReadKey();
        }
    }
}